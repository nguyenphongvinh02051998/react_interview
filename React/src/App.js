import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { actFetchPostAsync } from "./store/post/actions";
import TableList from "./components/TableList";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actFetchPostAsync());
  }, []);

  return (
    <div className="pt-3 pb-3 ml-1 mr-1">
      <div className="container">
        <TableList />
      </div>
    </div>
  );
}
export default App;
