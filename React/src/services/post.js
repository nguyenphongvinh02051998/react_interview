import {
  api
} from './api';

const postService = {
  getList(params) {
    return api.call().get('/posts', {
      params
    });
  }
}

export default postService;