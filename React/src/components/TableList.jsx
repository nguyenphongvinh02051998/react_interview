import React from "react";
import { useSelector } from "react-redux";
import { Table, Button } from "react-bootstrap";
export default function TableList() {
  const { posts } = useSelector((state) => state.Post);
  return (
    <Table>
      <thead>
        <tr>
          <th className="text-center">ID</th>
          <th>Name</th>
          <th>Body</th>
          <th width="140">Action</th>
        </tr>
      </thead>
      <tbody>
        {posts &&
          posts.map((item) => {
            return (
              <tr key={item.id}>
                <td className="text-center">{item.id}</td>
                <td>{item.title}</td>
                <td>{item.body}</td>
                <td width="140">
                  <Button variant="primary" size="sm" className="mr-1">edit</Button>
                  <Button variant="danger" size="sm">delete</Button>
                </td>
              </tr>
            );
          })}
      </tbody>
    </Table>
  );
}
