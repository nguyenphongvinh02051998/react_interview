import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import postReducer from './post/reducer';

const rootReducer = combineReducers({
  Post: postReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;