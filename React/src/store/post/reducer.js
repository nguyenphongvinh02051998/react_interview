import {
  ACT_FETCH_POST,
} from "./actions";

const intState = {
  posts: []
}

function reducer(postState = intState, action) {
  switch (action.type) {
    case ACT_FETCH_POST:
      return {
        ...postState,
        posts: action.payload.posts
      };
    default:
      return postState;
  }
}

export default reducer;