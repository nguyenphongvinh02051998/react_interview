import postService from "../../services/post";

// Action Type
export const ACT_FETCH_POST = 'ACT_FETCH_POST';


// Action
export function actFetchPost(posts) {
  return {
    type: ACT_FETCH_POST,
    payload: {
      posts
    }
  }
}


// Action Async
export function actFetchPostAsync() {
  return async (dispatch) => {
    try {
      const response = await postService.getList();
      const posts = response.data;
      dispatch(actFetchPost(posts));
    } catch (err) {
      // TODO
    }
  }
}