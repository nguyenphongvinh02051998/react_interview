<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function ajax_get_detail()
	{
		$id = $this->input->get('id');

		if (!$id) {
			die(json_encode(['status' => 0, 'data' => [], 'message' => 'Mã nhân viên không được bỏ trống']));
		}

		$item = $this->db->select('
			u.id,
			u.avatar,
			u.name,
			u.address,
			u.birthday,
			u.group_id,
			u.store_id,
			u.created,
			gr.name AS group_name,
			s.name AS store_name')
			->from('users AS u')
			->join('groups AS gr', 'gr.id = u.group_id', 'left')
			->join('stores AS s', 's.id = u.store_id', 'left')
			->where('u.id', $id)
			->get()->row();

		die(json_encode(['status' => 1, 'data' => $item, 'message' => 'success']));
	}

	public function ajax_save()
	{
		$id 			= $this->input->post('id');
		$name 		= $this->input->post('name');
		$store_id = $this->input->post('store_id') ?? 0;
		$group_id = $this->input->post('group_id') ?? 0;
		$address 	= $this->input->post('address');
		$birthday = $this->input->post('birthday');
		$avatar 	= null;

		if (empty($name)) {
			die(json_encode(['status' => 0, 'data' => [], 'message' => 'Tên nhân viên không được bỏ trống']));
		}

		if (!$birthday) {
			die(json_encode(['status' => 0, 'data' => [], 'message' => 'Ngày sinh không được bỏ trống']));
		}
		if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $birthday)) {
			die(json_encode(['status' => 0, 'data' => [], 'message' => 'Ngày sinh sai định dạng [YYYY-MM-DD]']));
		}

		if (!empty($_FILES['avatar'])) {
			$target_dir = "assets/upload/";

			$file = $_FILES['avatar'];
			$file_type = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
			$avatar = $target_dir . bin2hex(openssl_random_pseudo_bytes(10)) . ".$file_type";
			move_uploaded_file($file["tmp_name"], $avatar);
			$avatar = base_url($avatar);
		}

		$this->db->from('users')
			->set('store_id', $store_id)
			->set('group_id', $group_id)
			->set('address', $address)
			->set('birthday', $birthday ?? null)
			->set('name', $name)
			->set('avatar', $avatar);

		if ($id) {
			$flag = $this->db->where('id', $id)->update();
		} else {
			$this->db->insert();
			$flag = $this->db->insert_id();
		}
		if ($flag) {
			die(json_encode(['status' => 1, 'data' => $flag, 'message' => 'success']));
		}
		die(json_encode(['status' => 0, 'data' => $flag, 'message' => 'Lỗi hệ thống']));
	}

	public function ajax_get_list()
	{
		// user/ajax_get_list
		$limit 		= $this->input->get('limit');
		$offset 	= $this->input->get('offset');
		$store_id = $this->input->get('store_id');
		$group_id = $this->input->get('group_id');

		if (empty($limit) || !isset($offset)) {
			die(json_encode(['status' => 0, 'data' => [], 'message' => 'Trường limit và offset không được bỏ trống']));
		}

		$data = [
			'store_id' 	=> $store_id,
			'group_id' 	=> $group_id,
		];

		$query = $this->db->from('users AS u');
		$query = $this->_bind_get_list($query, $data);
		$results = $query->select('
			u.id,
			u.avatar,
			u.name,
			u.address,
			u.birthday,
			u.group_id,
			u.store_id,
			u.created,
			gr.name AS group_name,
			s.name AS store_name')
			->group_by('u.id')
			->limit($limit)
			->offset($offset)
			->get()->result();

		$query = $this->db->from('users AS u');
		$query = $this->_bind_get_list($query, $data);
		$total = $query->select('COUNT(DISTINCT u.id) AS total')->get()->row()->total;

		die(json_encode(['status' => 1, 'data' => ['list' => $results, 'total' => $total], 'message' => 'success']));
	}

	private function _bind_get_list($query, $data)
	{
		$query->join('groups AS gr', 'gr.id = u.group_id', 'left')
			->join('stores AS s', 's.id = u.store_id', 'left');

		if (!empty($data['group_id'])) {
			$query->where('gr.id', $data['group_id']);
		}
		if (!empty($data['store_id'])) {
			$query->where('s.id', $data['store_id']);
		}
		return $query;
	}

	public function ajax_get_user_detail()
	{
		// http://localhost/interview/user/ajax_get_user_detail
		$id 	= $this->input->get('id');
		$sql 	= "";
		$item 	= $this->db->query($sql)->row();
		die(json_encode(['status' => 1, 'data' => $item, 'message' => 'success']));
	}
}
