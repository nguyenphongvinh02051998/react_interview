<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function ajax_get_list()
	{
		$results = $this->db->from('groups')->get()->result();
		die(json_encode(['status' => 1, 'data' => $results, 'message' => 'success']));
	}
}
