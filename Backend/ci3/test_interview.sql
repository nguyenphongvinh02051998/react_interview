-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 31, 2022 lúc 06:55 PM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `test_interview`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 || 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `groups`
--

INSERT INTO `groups` (`id`, `name`, `active`) VALUES
(1, 'Nhân sự', 1),
(2, 'Kế toán', 1),
(3, 'Quản lý', 1),
(4, 'Lễ tân', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 || 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `stores`
--

INSERT INTO `stores` (`id`, `name`, `active`) VALUES
(1, 'Quận 10', 1),
(2, 'Bình Thạnh', 1),
(3, 'Thủ Đức', 1),
(4, 'Gò Vấp', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date NOT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `avatar`, `birthday`, `address`, `group_id`, `store_id`, `created`) VALUES
(1, 'Nguyễn Hoài An', 'http://localhost/baitapphongvan/interview/assets/upload/b86ac7a486a6fc5e29dc.png', '1998-04-01', '84 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 2, '2022-04-13 10:38:15'),
(2, 'Đặng Thùy Anh', NULL, '1998-05-09', '85 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 4, 1, '2022-04-13 10:38:15'),
(3, 'Phương Thảo', NULL, '1998-03-03', '85 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 3, '2022-04-21 09:29:34'),
(4, 'Mỹ Duyên', NULL, '1998-02-08', '4 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(5, 'Ngọc Bích', NULL, '1998-03-16', '5 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 0, '2022-04-21 09:29:34'),
(6, 'Ngọc Hoa', NULL, '1998-01-22', '6 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(7, 'Ngọc Diệp', NULL, '1998-04-13', '7 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(8, 'Ngọc Mai', NULL, '1998-02-25', '8 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 0, '2022-04-21 09:29:34'),
(9, 'Ngọc Trâm', NULL, '1998-04-29', '9 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(10, 'Nguyệt Minh', NULL, '1998-02-21', '10 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(11, 'Nguyệt Ánh', NULL, '1998-04-14', '11 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 2, '2022-04-21 09:29:34'),
(12, 'Quỳnh Chi', NULL, '1998-02-25', '12 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(13, 'Quỳnh Hương', NULL, '1998-04-21', '13 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 0, '2022-04-21 09:29:34'),
(14, 'Quỳnh Nhi', NULL, '1998-01-25', '14 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(15, 'Tú Linh', NULL, '1998-02-12', '15 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 0, '2022-04-21 09:29:34'),
(16, 'Thu Nguyệt', NULL, '1998-01-11', '16 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(17, 'Thanh Vân', NULL, '1998-02-14', '17 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(18, 'Thanh Trúc', NULL, '1998-01-30', '18 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 3, '2022-04-21 09:29:34'),
(19, 'Vân Trang', NULL, '1998-04-07', '19 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 2, '2022-04-21 09:29:34'),
(20, 'Kim Chi', NULL, '1998-03-24', '20 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 0, '2022-04-21 09:29:34'),
(21, 'Tố Như', NULL, '1998-01-01', '21 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 2, '2022-04-21 09:29:34'),
(22, 'Diệp Bích', NULL, '1998-04-08', '22 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(23, 'Mỹ Ngọc', NULL, '1998-03-27', '23 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 3, '2022-04-21 09:29:34'),
(24, 'Ngọc Hạ', NULL, '1998-01-28', '24 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(25, 'Tố Nga', NULL, '1998-04-08', '25 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 0, '2022-04-21 09:29:34'),
(26, 'Uyên Thư', NULL, '1998-01-03', '26 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(27, 'Bảo Thanh', NULL, '1998-03-14', '27 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 0, '2022-04-21 09:29:34'),
(28, 'Nhã Linh', NULL, '1998-04-28', '28 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(29, 'Gia Linh', NULL, '1998-01-30', '29 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 2, '2022-04-21 09:29:34'),
(30, 'Mẫn Nhi', NULL, '1998-04-14', '30 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(31, 'Minh Nguyệt', NULL, '1998-02-20', '31 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 1, '2022-04-21 09:29:34'),
(32, 'Minh Khuê', NULL, '1998-01-20', '32 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(33, 'Minh Tuệ', NULL, '1998-04-15', '33 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(34, 'Như Ý', NULL, '1998-01-25', '34 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(35, 'Tú Uyên', NULL, '1998-03-20', '35 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(36, 'Tuệ Mẫn', NULL, '1998-03-26', '36 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 2, '2022-04-21 09:29:34'),
(37, 'Tuệ Lâm', NULL, '1998-03-04', '37 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(38, 'Tuyết Lan', NULL, '1998-04-18', '38 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 0, '2022-04-21 09:29:34'),
(39, 'Tuệ Nhi', NULL, '1998-03-14', '39 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(40, 'Tú Anh', NULL, '1998-02-22', '40 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(41, 'Thùy Anh', NULL, '1998-02-26', '41 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 2, '2022-04-21 09:29:34'),
(42, 'Minh Anh', NULL, '1998-03-24', '42 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 3, '2022-04-21 09:29:34'),
(43, 'An Chi', NULL, '1998-01-29', '43 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(44, 'Hải Yến', NULL, '1998-02-17', '44 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 2, '2022-04-21 09:29:34'),
(45, 'Thảo Phương', NULL, '1998-04-27', '45 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 1, '2022-04-21 09:29:34'),
(46, 'Hương Tràm', NULL, '1998-04-13', '46 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 0, '2022-04-21 09:29:34'),
(47, 'Lệ Hằng', NULL, '1998-02-15', '47 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 1, '2022-04-21 09:29:34'),
(48, 'Ái Phương', NULL, '1998-04-30', '48 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(49, 'Bảo Quyên', NULL, '1998-03-28', '49 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(50, 'Bích Liên', NULL, '1998-03-10', '50 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(51, 'Diễm Châu', NULL, '1998-04-24', '51 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(52, 'Diễm My', NULL, '1998-01-21', '52 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 0, '2022-04-21 09:29:34'),
(53, 'Diễm Kiều', NULL, '1998-02-01', '53 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(54, 'Diễm Phương', NULL, '1998-01-15', '54 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(55, 'Diễm Thảo', NULL, '1998-03-16', '55 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 2, '2022-04-21 09:29:34'),
(56, 'Đông Nghi', NULL, '1998-03-21', '56 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 3, '2022-04-21 09:29:34'),
(57, 'Đan Thanh', NULL, '1998-04-10', '57 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(58, 'Gia Mỹ', NULL, '1998-03-14', '58 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 2, '2022-04-21 09:29:34'),
(59, 'Huyền Anh', NULL, '1998-04-15', '59 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 2, '2022-04-21 09:29:34'),
(60, 'Hồng Nhung', NULL, '1998-01-26', '60 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 0, '2022-04-21 09:29:34'),
(61, 'Kim Liên', NULL, '1998-02-06', '61 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 1, '2022-04-21 09:29:34'),
(62, 'Kim Oanh', NULL, '1998-04-24', '62 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(63, 'Khánh Quỳnh', NULL, '1998-03-17', '63 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 2, '2022-04-21 09:29:34'),
(64, 'Thảo Chi', NULL, '1998-02-17', '64 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(65, 'Thiên Thanh', NULL, '1998-04-10', '65 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 3, '2022-04-21 09:29:34'),
(66, 'Thục Quyên', NULL, '1998-04-09', '66 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 0, '2022-04-21 09:29:34'),
(67, 'Thục Trinh', NULL, '1998-01-10', '67 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 1, '2022-04-21 09:29:34'),
(68, 'Hương Chi', NULL, '1998-04-19', '68 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(69, 'Mỹ Dung', NULL, '1998-02-18', '69 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(70, 'Lan Hương', NULL, '1998-04-07', '70 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 1, '2022-04-21 09:29:34'),
(71, 'Mỹ Lệ', NULL, '1998-02-03', '71 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 3, '2022-04-21 09:29:34'),
(72, 'Cát Tiên', NULL, '1998-04-18', '72 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(73, 'Anh Thư', NULL, '1998-01-12', '73 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 1, '2022-04-21 09:29:34'),
(74, 'Thanh Tú', NULL, '1998-01-27', '74 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 3, '2022-04-21 09:29:34'),
(75, 'Tú Vi', NULL, '1998-02-03', '75 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(76, 'Hạ Vũ', NULL, '1998-02-07', '76 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(77, 'Mộc Miên', NULL, '1998-02-16', '77 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 2, '2022-04-21 09:29:34'),
(78, 'Hoài Phương', NULL, '1998-04-19', '78 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(79, 'Hồng Diễm', NULL, '1998-04-04', '79 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 3, '2022-04-21 09:29:34'),
(80, 'Bích Thảo', NULL, '1998-01-01', '80 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 1, '2022-04-21 09:29:34'),
(81, 'Bích Thủy', NULL, '1998-01-24', '81 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(82, 'Ðoan Trang', NULL, '1998-03-15', '82 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 0, '2022-04-21 09:29:34'),
(83, 'Đan Tâm', NULL, '1998-03-25', '83 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 2, '2022-04-21 09:29:34'),
(84, 'Hiền Nhi', NULL, '1998-01-16', '84 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 3, '2022-04-21 09:29:34'),
(85, 'Hiền Thục', NULL, '1998-04-12', '85 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 1, '2022-04-21 09:29:34'),
(86, 'Hương Thảo', NULL, '1998-04-14', '86 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 1, 0, '2022-04-21 09:29:34'),
(87, 'Minh Tâm', NULL, '1998-01-22', '87 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 2, 3, '2022-04-21 09:29:34'),
(88, 'Mỹ Tâm', NULL, '1998-01-16', '88 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 0, 2, '2022-04-21 09:29:34'),
(89, 'Phương Thùy', NULL, '1998-03-13', '89 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(90, 'Phương Trinh', NULL, '1998-01-21', '90 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 0, '2022-04-21 09:29:34'),
(91, 'Nhã Phương', NULL, '1998-02-13', '91 Nguyễn Háo Vĩnh, Tân Quý, Tân Phú, TPHCM', 3, 3, '2022-04-21 09:29:34'),
(92, 'Văn D', 'http://localhost/baitapphongvan/interview/assets/upload/181596bf14423b28b8c0.png', '0000-00-00', NULL, 3, 2, '2022-08-31 23:39:23'),
(93, 'Văn E', 'http://localhost/baitapphongvan/interview/assets/upload/e9e035482d4bb566a4a7.png', '0000-00-00', NULL, 3, 2, '2022-08-31 23:41:10'),
(94, 'Văn E', 'http://localhost/baitapphongvan/interview/assets/upload/c820fd12ed36218fdf92.png', '0000-00-00', NULL, 3, 2, '2022-08-31 23:41:32'),
(95, 'Văn E', 'http://localhost/baitapphongvan/interview/assets/upload/9e5365399ec02ad7d55e.png', '2022-01-01', NULL, 3, 2, '2022-08-31 23:44:05'),
(96, 'Văn E', 'http://localhost/baitapphongvan/interview/assets/upload/518af0fc907ccfa493e4.png', '2022-01-01', NULL, 0, 0, '2022-08-31 23:45:58');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
